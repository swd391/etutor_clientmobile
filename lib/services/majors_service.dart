import 'package:etutor/data/majors_model.dart';
import 'package:etutor/services/http_service.dart';
import 'package:flutter/material.dart';

abstract class MajorsService {
  static List<MajorsGroup> currentListMajorsGroup;

  static Future<List<MajorsGroup>> getMajors(BuildContext context) async {
    return HttpService.getWithAuth(
      context,
      'http://api.etutor.top/ETutor/api/majors/getListMajors',
    ).then((response) {
      currentListMajorsGroup = response.map<MajorsGroup>((e) => MajorsGroup.fromJson(e)).toList();
      return currentListMajorsGroup;
    });
  }
}