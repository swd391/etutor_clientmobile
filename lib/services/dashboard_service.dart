import 'package:etutor/data/dashboard_model.dart';
import 'package:etutor/services/authentication_service.dart';
import 'package:etutor/services/http_service.dart';
import 'package:flutter/material.dart';

abstract class DashboardService {
  static List<DashboardGroup> currentListDashboardGroup;

  static Future<List<DashboardGroup>> getDashboard(BuildContext context) async {
    return HttpService.getWithAuth(
      context,
      'http://api.etutor.top/ETutor/api/dashboard',
    ).then((response) {
      AuthenticationService.setDashboardLatestLoad();
      currentListDashboardGroup = response.map<DashboardGroup>((e) => DashboardGroup.fromJson(e)).toList();
      return currentListDashboardGroup;
    });
  }
}